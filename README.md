# DNS

## Generally
```
virtualenv env
source env/bin/activate
pip install -r requirements.txt

mkdir secrets
cat > secrets/production.env

octodns-validate --config-file=config/production.yaml
octodns-sync --config-file=config/production.yaml
octodns-sync --config-file=config/production.yaml --doit
```
## Gitlab CI method

< Make changes >

```
git push origin master
```

Make sure built successfully https://gitlab.com/doubtingben/dns/pipelines

Tag, and push tag

```
git tag 1.0
git push origin tag 1.0
```

Check build status again.
